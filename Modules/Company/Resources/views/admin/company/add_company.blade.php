@extends('layouts.admin_design')
@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>ADD COMPANY
            </div>
        </div>
    </div>
</div>
<hr>
@if(count($errors)>0)
<div class="alert alert-error alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong class="strmsg">
    <p>{{ $errors->first('name') }}</p>
    <p>{{ $errors->first('email') }}</p>
    <p>{{ $errors->first('logo') }}</p>
    <p>{{ $errors->first('website') }}</p>
    </strong>
</div>
@endif
@if(Session::has('flash_message_error'))
<div class="alert alert-error alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!! session('flash_message_error') !!}</strong>
</div>
@endif
<div class="main-card mb-3 card">
    <div class="card-body">
        <form enctype="multipart/form-data" action="{{ url('/admin/companies/add') }}" method="post">
            @csrf
            <div class="position-relative row form-group">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input name="name" id="name" placeholder="name" type="text" class="form-control" autofocus autocomplete="name" value="{{old('name')}}">
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input name="email" id="email" placeholder="email" type="text" class="form-control" value="{{old('email')}}">
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="logo" class="col-sm-2 col-form-label">Logo</label>
                <div class="col-sm-10">
                    <input name="logo" id="logo" type="file" class="form-control-file">
                    <small class="form-text text-muted">minimum 100x100</small>
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="website" class="col-sm-2 col-form-label">Website</label>
                <div class="col-sm-10">
                    <input name="website" id="website" placeholder="website" type="text" class="form-control" value="{{old('website')}}">
                </div>
            </div>
            <div class="position-relative row form-check">
                <div class="col-sm-10 offset-sm-2">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
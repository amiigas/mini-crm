@extends('layouts.admin_design')
@section('content')
<link href="{{url('css/jquery.dataTables.min.css')}}" rel="stylesheet">
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>COMPANY
            </div>
        </div>
    </div>
</div>
<hr>
<div class="app-page-title createnewcompany">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>
                <a href="{{url('admin/companies/add')}}"><button class="btn btn-success pull-right btnnewcompany">Create new company</button></a>
            </div>
        </div>
    </div>
</div>
@if(Session::has('flash_message_success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!! session('flash_message_success') !!}</strong>
</div>
@endif
@if(Session::has('flash_message_error'))
<div class="alert alert-error alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!! session('flash_message_error') !!}</strong>
</div>
@endif
<div class="main-card mb-3 card">
    <div class="card-body">
        <table id="example2" class="table table-hover table-striped table-bordered">  
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Logo</th>
                    <th>Website</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($companies as $company)
                <tr>
                    <td>{{$company->name }}</td>
                    <td>{{ $company->email }}</td>
                    <td><img width="60" src="{{url('storage').'/'.$company->logo}}"/></td>
                    <td>{{$company->website}}</td>
                    <td>
                        <a href="{{ url('admin/companies/'.$company->id.'/edit') }}" class="btn btn-primary btn-mini" title="Edit Company">Edit</a>
                        <a href="{{ url('admin/companies/'.$company->id.'/delete') }}" class="btn btn-danger btn-mini delConfirm" title="Delete Company">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script src="{{url('js/jquery.dataTables.min.js')}}"></script>
@endsection
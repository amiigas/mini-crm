<?php

namespace Modules\Company\Http\Controllers;

use Validator;
use Modules\Company\Entities\Company;
use Modules\Core\Entities\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $companies = Company::all();

        return view('company::admin.company.manage_company')->with(compact('companies'))->withTitle('Manage Company');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('company::admin.company.add_company')
                ->withTitle('Add Company');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if($request->isMethod('post'))
        {

            $rules = array(
                'name' => ['required', 'max:64'],
                'email' => ['nullable', 'email', 'max:64', 'unique:users'],
                'logo' => ['nullable', 'mimes:jpeg,jpg,png,gif'],
                'website' => ['nullable', 'max:64'],
            );

            // checking all field
            $validator = Validator::make($request->all() , $rules);

            // if the validator fails, redirect back to the form
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator)
                ->withInput($request->all());
            }
            else
            {
                //upload image
                if ($request->hasFile('logo'))
                {
                    $image_tmp = $request->logo;
                    if ($image_tmp->isValid())
                    {
                        $fileinfo = @getimagesize($_FILES["logo"]["tmp_name"]);
                        $width = $fileinfo[0];
                        $height = $fileinfo[1];
                        if($width<100 && $height<100)
                        {
                            return redirect()->back()->with('flash_message_error', 'Logo\'s width and height should be equal or greater than 100')
                                ->withInput($request->all());
                        }

                        $extension = $image_tmp->getClientOriginalExtension();
                        $filename = rand(111, 99999) . '.' . $extension;

                        // request()
                        //     ->logo
                        //     ->move(public_path('/') , $filename);

                        $request->file('logo')->storeAs('/', $filename  ,'public');

                        $logo = $filename;
                    }
                    else
                    {
                        return redirect()->back()->withErrors('logo', 'Image is not valid.')
                            ->withInput($request->all());

                    }
                }
                else
                {
                    $logo = "";
                }
                
                Company::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'logo' => $logo,
                    'website' => $request->website
                ]);

                $companies = Company::all();
                return redirect('admin/companies')->with('flash_message_success', 'Company added successfully.')
                    ->withTitle('Manage Company')
                    ->with(compact('companies'));

            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        if(!$company)
            return redirect('admin/companies')->with('flash_message_error', 'Edit failed. Cannot find company.');

        return view('company::admin.company.edit_company')
            ->withTitle('Edit Company')
            ->with(compact('company'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if($request->isMethod('post'))
        {

            $rules = array(
                'name' => ['nullable', 'max:64'],
                'email' => ['nullable', 'email', 'max:64', 'unique:users,email,'.$id],
                'logo' => ['nullable', 'mimes:jpeg,jpg,png,gif'],
                'website' => ['nullable', 'max:64'],
            );

            // checking all field
            $validator = Validator::make($request->all() , $rules);

            // if the validator fails, redirect back to the form
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator)
                ->withInput($request->all());
            }
            else
            {
                $company = Company::find($id);

                //upload image
                if ($request->hasFile('logo'))
                {
                    $image_tmp = $request->logo;
                    if ($image_tmp->isValid())
                    {
                        $fileinfo = @getimagesize($_FILES["logo"]["tmp_name"]);
                        $width = $fileinfo[0];
                        $height = $fileinfo[1];
                        if($width<100 && $height<100)
                        {
                            return redirect()->back()->with('flash_message_error', 'Logo\'s width and height should be equal or greater than 100')
                                ->withInput($request->all());
                        }

                        $extension = $image_tmp->getClientOriginalExtension();
                        $filename = rand(111, 99999) . '.' . $extension;

                        // request()
                        //     ->logo
                        //     ->move(public_path('/') , $filename);

                        $request->file('logo')->storeAs('/', $filename  ,'public');

                        if(!empty($company->logo))
                        {
                            if (file_exists(public_path('/storage/' . $company->logo)))
                            {
                                unlink(public_path('/storage/' . $company->logo));
                            }
                        }

                        $company->logo = $filename;

                        $company->update($company->only('logo'));
                    }
                    else
                    {
                        return redirect()->back()->withErrors('logo', 'Image is not valid.')
                            ->withInput($request->all());

                    }
                }

                $request->only($company->getFillable());
                $company->fill(array_filter($request->except('_token','logo')));
                $company->save();

                return redirect('admin/companies')->with('flash_message_success', 'Company updated successfully.');

            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        if(!$company)
            return redirect('admin/companies')->with('flash_message_error', 'Delete failed. Cannot find company.');

        if (!empty($company->logo))
        {
            if (file_exists(public_path('/storage/' . $company->logo)))
            {
                unlink(public_path('/storage/' . $company->logo));
            }
        }

        $company->delete();

        return redirect('admin/companies')->with('flash_message_success', 'Company deleted successfully.');
    }
}

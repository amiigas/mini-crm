<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {
	//Company
    Route::prefix('companies')->group(function () {
        Route::get('/', 'CompanyController@index');
        Route::get('/add', 'CompanyController@create');
        Route::post('/add', 'CompanyController@store');
        Route::get('/{id}/edit', 'CompanyController@edit');
        Route::post('/{id}/edit', 'CompanyController@update');
        Route::get('/{id}/delete', 'CompanyController@destroy');
    });
});
<?php
   
namespace App\Http\Controllers\Api;
   
use DB;
use Validator;
use App\OauthAcessToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
   
class ApiLoginController extends Controller
{
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['name'] =  $user->name;
   
            return $this->response($success, 200, 'User logged in successfully.');
        } 
        else{ 
            return $this->response(null, 401, 'Unauthorised.');
        } 
    }

    public function logout(Request $request)
    {
        if (Auth::check()) {
            DB::table('oauth_access_tokens')
                ->where('user_id', Auth::user()->id)
                ->update([
                    'revoked' => true
                ]);
            return $this->response(null, 200, 'Successfully logged out from all devices.');
        }
        return $this->response(null, 500, 'Log out failed.');
    }
}
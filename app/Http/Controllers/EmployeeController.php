<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Company;
use Validator;
use Illuminate\Http\Request;
use App\Services\EmployeeService;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    protected $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        try {
            $limit = $request->limit ? $request->limit : 10;
            $employees = $this->employeeService->all($limit);
            return $this->response($employees, 200, 'Successfully retrieved employees.');
        } catch (\Exception $ex) {
            return $this->response(null, 500, $ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = $this->validate($request, [
                'first_name' => ['required', 'max:32', 'string'],
                'last_name' => ['required', 'max:32', 'string'],
                'company' => ['nullable', 'integer', 'exists:companies,id'],
                'email' => ['nullable', 'email', 'max:64', 'unique:employees'],
                'phone' => ['nullable', 'max:32'],
            ]);
        } catch (\Exception $e) {
            return $this->response($e->errors(), 422, $e->getMessage());
        }

        try {
            $employee = $this->employeeService->create($request->only($this->employeeService->fillable()));
            return $this->response($employee, 200, 'Employee successfully created.');
        } catch (\Exception $ex) {
            return $this->response(null, 500, $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $employee = $this->employeeService->find($id);
            return $this->response($employee, 200, 'Successfully retrieved employee.');
        } catch (\ModelNotFoundException $ex) {
            return $this->response(null, 404, $ex->getMessage());
        } catch (\Exception $ex) {
            return $this->response(null, 500,$ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validate = $this->validate($request, [
                'first_name' => ['filled', 'max:32', 'string'],
                'last_name' => ['filled', 'max:32', 'string'],
                'company' => ['nullable', 'integer', 'exists:companies,id'],
                'email' => ['nullable', 'email', 'max:64', 'unique:employees,email,'.$id],
                'phone' => ['nullable', 'max:32'],
            ]);
        } catch (\Exception $e) {
            return $this->response($e->errors(), 422, $e->getMessage());
        }

        try {
            $employee = $this->employeeService->update($id, $request->only($this->employeeService->fillable()));
            return $this->response($employee, 200, 'Successfully updated employee.');
        } catch (\ModelNotFoundException $ex) {
            return $this->response(null,404, $ex->getMessage());
        } catch (\Exception $ex) {
            return $this->response(null, 500, $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $employee = $this->employeeService->destroy($id);
            return $this->response($employee,200,'Employee Deleted Successfully.');
        } catch (\ModelNotFoundException $ex) {
            return $this->response(null, 404,$ex->getMessage());
        } catch (\Exception $ex) {
            return $this->response(null, 500,$ex->getMessage());
        }
    }

    public function updateCompany(Request $request)
    {
        $input = $request->all();

        // $hi=array('hello'=>$input['employees']);
        // echo json_encode($hi,true);
        // exit;
        foreach ($input['employees'] as $key => $value) {
            $comemp = explode("-", $value);
            $employee_id = $comemp[1];
            Employee::where('id',$employee_id)->update(['company'=>null]);
        }

        foreach ($input['companies'] as $key => $value) {

            $empcom = explode("-", $value);
            $ec = $empcom[0];
            if($ec=="C")
            {
                $company_id = $empcom[1];
            }
            elseif($ec=="E")
            {
                $employee_id = $empcom[1];
                Employee::where('id',$employee_id)->update(['company'=>$company_id]);
            }

        }

        return response()->json(['status'=>'success']);
    }
}

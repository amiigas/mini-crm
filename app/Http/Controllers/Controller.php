<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response($response, $httpStatusCode = 200, $message = null)
    {
        if ((int)substr($httpStatusCode, 0, -2) === 2) {
            $error = false;
        } else {
            $error = true;
        }
        return response()->json($this->responseFormat($httpStatusCode, $error, $response, $message), $httpStatusCode);
    }

    private function responseFormat($httpStatusCode, $error, $response, $message)
    {
        return [
            'code' => $httpStatusCode,
            'error' => $error,
            'message' => $message,
            'response' => $response
        ];
    }
}

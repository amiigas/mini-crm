<?php

namespace App\Services;

use App\Employee;

class EmployeeService
{
    protected $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function fillable()
    {
        return $this->employee->getFillable();
    }

    public function all($limit)
    {
        return $this->employee->with("company")->orderBy('id', 'desc')->paginate($limit);
    }

    public function find($id)
    {
        return $this->employee->with("company")->findOrFail($id);
    }

    public function create(array $attributes)
    {
        $employee = $this->employee->newInstance();
        $employee->fill($attributes);
        $employee->save();
        return $employee;
    }

    public function update($id, array $attributes)
    {
        $employee = $this->employee->findOrFail($id);
        $employee->fill($attributes);
        $employee->save();
        return $employee;
    }

    public function destroy($id)
    {
        $employee = $this->employee->findOrFail($id);
        $employee->delete();
        return $employee;
    }
}
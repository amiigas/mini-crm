-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 05:41 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mini-crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `logo`, `website`, `created_at`, `updated_at`) VALUES
(1, 'Amnil Techonology', 'amniltech@gmail.com', '7487.jpg', 'mini-crm', '2020-02-27 11:41:31', '2020-02-27 11:41:31'),
(2, 'Geniussystems', 'geniussystems@gmail.com', '58611.jpg', 'ems', '2020-02-27 13:01:51', '2020-02-28 01:59:04'),
(3, 'Chaudhary Group', 'cg@gmail.com', '71920.jpg', 'cgdigital', '2020-02-28 01:59:56', '2020-02-28 01:59:56');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` bigint(20) UNSIGNED DEFAULT NULL,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `company`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(2, 'Ram', 'Kumar', 1, 'ram@gmail.com', '9860313727', '2020-02-27 13:23:38', '2020-02-28 10:11:35'),
(3, 'Shyam', 'Kumar', 2, 'shyam@gmail.com', '9860313717', '2020-02-27 13:23:55', '2020-02-28 10:11:35'),
(4, 'Hari', 'Kumar', 1, 'hari@gmail.com', '9860113717', '2020-02-27 13:24:18', '2020-02-28 10:11:35'),
(5, 'Raghav', 'Kumar', NULL, 'raghav@gmail.com', '9860123717', '2020-02-27 13:24:37', '2020-02-28 10:11:35'),
(6, 'Manoj', 'Kumar', NULL, 'manoj@gmail.com', '9860125717', '2020-02-27 13:24:56', '2020-02-28 10:11:35'),
(7, 'Gopal', 'Kumar', 3, 'gopal@gmail.com', '9860334432', '2020-02-27 13:34:56', '2020-02-28 10:11:35'),
(8, 'Lal', 'Kumar', NULL, 'lal@gmail.com', '9841334233', '2020-02-27 13:34:56', '2020-02-28 10:11:35'),
(9, 'Raghu', 'Kumar', NULL, 'raghu@gmail.com', '9841223456', '2020-02-27 17:24:56', '2020-02-28 10:11:35'),
(10, 'Rajesh', 'Kumar', NULL, 'rajesh@gmail.com', '9843223456', '2020-02-28 01:40:44', '2020-02-28 10:11:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2020_02_27_054125_create_companies_table', 2),
(4, '2020_02_27_152919_create_employees_table', 3),
(5, '2016_06_01_000001_create_oauth_auth_codes_table', 4),
(6, '2016_06_01_000002_create_oauth_access_tokens_table', 4),
(7, '2016_06_01_000003_create_oauth_refresh_tokens_table', 4),
(8, '2016_06_01_000004_create_oauth_clients_table', 4),
(9, '2016_06_01_000005_create_oauth_personal_access_clients_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('10bf1c18f51b995aa65c3f60f932275ae5c360461260c6b8f8134d727291a772165914f89beaa5a1', 1, 1, 'MyApp', '[]', 1, '2020-02-27 11:30:10', '2020-02-27 11:30:10', '2021-02-27 17:15:10'),
('136adeffc8f168e6885c1dc3a0993bc07c942107919a093a7f7b62ffc81a343b42ed26523951fed2', 1, 1, 'MyApp', '[]', 1, '2020-02-27 11:07:22', '2020-02-27 11:07:22', '2021-02-27 16:52:22'),
('36f029f237eba2e86c6e52587971868c7c225032654bfb30ca7fda631f4425ec213d91f1118e5457', 1, 1, 'MyApp', '[]', 1, '2020-02-27 12:24:19', '2020-02-27 12:24:19', '2021-02-27 18:09:19'),
('442209ddc30734914223983daabdf96eae9abc9d66bbc79b1f8a01a009b38459b971d4e45bb52473', 1, 1, 'MyApp', '[]', 1, '2020-02-27 11:29:05', '2020-02-27 11:29:05', '2021-02-27 17:14:05'),
('51178d8ce52374426308275cf6f0e413a6ac6af0bca2421c0b68878450a1f22a002eda6af1535d5f', 1, 1, 'MyApp', '[]', 1, '2020-02-28 01:37:57', '2020-02-28 01:37:57', '2021-02-28 07:22:57'),
('5fc4b15a7a13461706be39e16f47b25496228ea5e6d4b7739476a9db730eeb0437849475c09d89ad', 1, 1, 'MyApp', '[]', 0, '2020-02-28 09:13:47', '2020-02-28 09:13:47', '2021-02-28 14:58:47'),
('7a3c5dd1f4d49e8e7b514bea74075948534906e2ff6e45b884ab39558ae183e97f819332c4f79fa6', 1, 1, 'MyApp', '[]', 1, '2020-02-27 11:12:49', '2020-02-27 11:12:49', '2021-02-27 16:57:49'),
('a2d714b70d88a8ef755b00321eac0c13e444bed73067745387a3e4b7195059f2e7cef699b2332c83', 1, 1, 'MyApp', '[]', 1, '2020-02-27 11:22:16', '2020-02-27 11:22:16', '2021-02-27 17:07:16'),
('aec0135b9f9fb18c1d4677df8880eb0017c4885b79bcdf591c3c9ab801692b446738d958411f251f', 1, 1, 'MyApp', '[]', 1, '2020-02-28 01:37:10', '2020-02-28 01:37:10', '2021-02-28 07:22:10'),
('b34094f348789d369ad8aa81ffc0f6f40414f7b21a7a604bfa1fac34a7a253cc1147147a3e3cc615', 1, 1, 'MyApp', '[]', 1, '2020-02-27 11:18:02', '2020-02-27 11:18:02', '2021-02-27 17:03:02'),
('df6a8c253d733a3cef00bbde5520bb0984119037b5afda098f6b6bbfdc0ce19806ba28442a6b5cc0', 1, 1, 'MyApp', '[]', 1, '2020-02-27 11:21:19', '2020-02-27 11:21:19', '2021-02-27 17:06:19'),
('e56c40c7e5e6ebde44b67582f2d378479eab2baa7bbfb275a78b1579e6afbdad17fbd88e7329040f', 1, 1, 'MyApp', '[]', 1, '2020-02-28 01:36:54', '2020-02-28 01:36:54', '2021-02-28 07:21:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'wbfjllcekCJkArTQTecO7IeWHglS2mZ2VqbgiSeC', 'http://localhost', 1, 0, 0, '2020-02-27 11:02:04', '2020-02-27 11:02:04'),
(2, NULL, 'Laravel Password Grant Client', 'UfHt3MVEl9RjTXWynjNWmeJYkg0PpiWLCk5PgzKB', 'http://localhost', 0, 1, 0, '2020-02-27 11:02:04', '2020-02-27 11:02:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-02-27 11:02:04', '2020-02-27 11:02:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Pinkie Daugherty', 'admin@admin.com', '$2y$10$UaPNS2LEwA/rQr/mCUpLreu4m/Xc6xYaAnrdNhbCTEpeocHq8Udga', '2020-02-25 12:12:04', '2020-02-25 12:12:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_email_unique` (`email`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD KEY `employees_company_foreign` (`company`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_company_foreign` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

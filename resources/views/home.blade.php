@extends('layouts.fordashonly')
@section('content')
	<div class="app-sidebar sidebar-shadow">
	    <div class="app-header__logo">
	        <div class="logo-src"></div>
	        <div class="header__pane ml-auto">
	            <div>
	                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
	                    <span class="hamburger-box">
	                        <span class="hamburger-inner"></span>
	                    </span>
	                </button>
	            </div>
	        </div>
	    </div>
	    <div class="app-header__mobile-menu">
	        <div>
	            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
	                <span class="hamburger-box">
	                    <span class="hamburger-inner"></span>
	                </span>
	            </button>
	        </div>
	    </div>
	    <div class="app-header__menu">
	        <span>
	            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
	                <span class="btn-icon-wrapper">
	                    <i class="fa fa-ellipsis-v fa-w-6"></i>
	                </span>
	            </button>
	        </span>
	    </div>
	    <div class="scrollbar-sidebar">
	        <div class="app-sidebar__inner">
	            <ul class="vertical-nav-menu">
	                <li @if(Request::segment(1)=="home"){{'class=mm-active'}}@endif>
	                    <a href="{{url('/home')}}">
	                        Dashboard
	                    </a>
	                </li>
	                <li @if(Request::segment(2)=="companies"){{'class=mm-active'}}@endif>
	                    <a href="{{url('admin/companies')}}">
	                        Company
	                    </a>
	                </li>
	                <li @if(Request::segment(2)=="employees"){{'class=mm-active'}}@endif>
	                    <a href="#" aria-expanded="false">
                            Employee
                        </a>
                        <ul class="mm-collapse" style="height: 7.04px;">
                            <ul class="list-group shadow-lg connectedSortable" id="padding-item-drop">
			                  @if(!empty($employees) && $employees->count())
			                    @foreach($employees as $key=>$value)
			                    	@if(empty($value->company) || is_null($value->company))
			                        <li class="list-group-item" item-id="E-{{ $value->id }}">Employee {{ $value->id }}</li>
			                        @endif
			                    @endforeach
			                  @endif
			                </ul>
                        </ul>
	                </li>
	            </ul>
	        </div>
	    </div>
	</div>
<div class="app-main__outer">
    <div class="app-main__inner">	
    	<?php $i=1;?>
    	@foreach($companies as $company)
    		<?php if($i==1 && $i<4){?>
    	<div class="row">
    		<?php }?>
			<div class="col-md-4 col-lg-4 col-xl-4">
				<div class="card-hover-shadow profile-responsive card-border border-success mb-3 card">
					<div class="dropdown-menu-header">
			            <div class="dropdown-menu-header-inner bg-success">
			                <div class="menu-header-content">
			                    <div class="avatar-icon-wrapper btn-hover-shine mb-2 avatar-icon-xl">
			                        <div class="avatar-icon rounded"><img src="{{url('/storage/')}}/{{$company->logo}}" alt="Avatar 6"></div>
			                    </div>
			                    <div><h5 class="menu-header-title">{{$company->name}}</h5>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <div class="scroll-area-sm">
			            <div class="scrollbar-container ps ps--active-y">
			                <ul class="list-group list-group-flush">
			                    <li class="list-group-item">
			                        <div class="widget-content p-0">
			                            <div class="widget-content-wrapper">
			                                <div class="widget-content-left"><i class="pe-7s-file text-muted fsize-2"></i></div>
			                                <div class="widget-content-left">
			                                    <ul class="list-group  connectedSortable" id="complete-item-drop" item-id="{{ $company->id }}">
			                                    	<li style="visibility: none;" class="list-group-item " item-id="C-{{ $company->id }}"></li>
								                  @if(!empty($employees) && $employees->count())
								                    @foreach($employees as $key=>$value)
								                    	@if($value->company==$company->id)
								                        <li class="list-group-item " item-id="E-{{ $value->id }}">Employee {{ $value->id }}</li>
								                    	@endif
								                    @endforeach
								                  @endif
								                </ul>
			                                </div>
			                            </div>
			                        </div>
			                    </li>
			                </ul>
			            </div>
			        </div>
			        <div class="text-center d-block card-footer">
			        </div>
			    </div>
			</div>
			<?php $i++?>
			<?php if($i==4){?>
		</div>
			<?php $i=1;}?>
		@endforeach
	</div>
</div>
<script src="{{url('js/jquery-ui.js')}}"></script>
<script>
  $( function() {
    $( "#padding-item-drop, #complete-item-drop" ).sortable({
      connectWith: ".connectedSortable",
      opacity: 0.5,
    }).disableSelection();

    $( ".connectedSortable" ).on( "sortupdate", function( event, ui ) {
        var employees = [];
        var companies = [];
        var company_id = '';

        $("#padding-item-drop li").each(function( index ) {
            employees[index] = $(this).attr('item-id');
        });

        $("#complete-item-drop li").each(function( index ) {
        	company_id = $("#complete-item-drop").attr('item-id');
            companies[index] = $(this).attr('item-id');
        });

        if(location.hostname === "localhost" || location.hostname === "127.0.0.1") {
			var base_url = "http://localhost/mini-crm/";
		} else {
			var base_url = location.host;
		}

		var jsonData = {
			'employees': employees,
			'companies': companies
		};

        $.ajax({
            url      : base_url + "public/admin/employees/updateCompany",
            type     : "POST",
            dataType : "json",
            data     : jsonData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success  : function( msg ) {
            	console.log('success');
            },
            error    : function (XMLHttpRequest, textStatus, errorThrown) {
                alert( errorThrown );
                alert("theres an error with AJAX");
            }
        });
    });
  });
</script>
@endsection
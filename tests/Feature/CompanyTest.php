<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\Company\Entities\Company;
use Tests\TestCase;
use App\User;


class CompanyTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_is_redirected_to_login_page_when_not_authenticated()
    {
        $response = $this->get('/admin/companies');

        $response->assertRedirect('/login');
    }

    public function test_get_companies()
    {
        $user = factory(User::class)->make();

        $company = factory(Company::class)->create();

        //When user visit the companies page
        $response = $this->actingAs($user)->get('/admin/companies'); // your route to get companies
 
        //He should be able to read the companies
        $response->assertSuccessful();
        $response->assertSee($company->name);
    }

    public function test_create_company()
    {   
        $user = factory(User::class)->create();

        $company = factory(Company::class)->create();
     
        //When user submits company request to create endpoint
        $this->actingAs($user)->post('/admin/companies/add',$company->toArray());
     
        //It gets stored in the database
        $this->assertEquals(1,Company::all()->count());
    }

    public function test_update_company()
    {
        $user = factory(User::class)->create();

        $company = factory(Company::class)->create();
        $company['name'] = 'Updated Name';
        $company['email'] = '';
        $company['logo'] = '';
        $company['website'] = '';

        $this->actingAs($user)->post('/admin/companies/'.$company->id.'/edit', $company->toArray()); // your route to update company
        //The company should be updated in the database.

        $this->assertDatabaseHas('companies',['id'=>$company->id , 'name'=>'Updated Name']);
    }

    public function test_delete_company()
    {
        $user = factory(User::class)->create();

        $company = factory(Company::class)->create();
     
        $this->actingAs($user)->get('/admin/companies/'.$company->id.'/delete');
        //The company should be deleted from the database.
     
        $this->assertDatabaseMissing('companies',['id'=> $company->id]);
    }

}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('admin/login', 'Api\ApiLoginController@login');

Route::prefix('admin')->middleware('auth:api')->group(function () {
    Route::resource('employees', 'EmployeeController');
	Route::post('logout', 'Api\ApiLoginController@logout');
});